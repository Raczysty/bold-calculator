<?php
/**
 *
 * @wordpress-plugin
 * Plugin Name:       Bold Calculator
 * Description:       Wtyczka wyliczająca kwotę brutto i kwotę podatku.
 * Version:           1.0.0
 * Author:            Sebastian Raczkowski
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       bold-calculator
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'BOLD_CALC_VERSION', '1.0.0' );

require plugin_dir_path( __FILE__ ) . 'includes/class-bold-calculator.php';
require plugin_dir_path( __FILE__ ) . 'includes/class-bold-calculator-cpt.php';
function activateCalculator() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-bold-calculator-activator.php';
	CalculatorActivator::activate();
}

function deactivateCalculator() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-bold-calculator-deactivator.php';
	CalculatorDeactivator::deactivate();
}

register_activation_hook( __FILE__, 'activateCalculator' );
register_deactivation_hook( __FILE__, 'deactivateCalculator' );

function runCalculator() {
	$plugin = new Calculator();
	$plugin->run();
}

runCalculator();

add_action( 'admin_menu', 'calculatorMenu' );

function calculatorMenu() {
	add_options_page( 'Bold Calculator Options', 'Bold Calculator Options', 'manage_options', 'bold-calculator', 'calculatorOptions' );
}

function calculatorOptions() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'Przykro mi ale nie masz odpowiednich uprawnień.' ) );
	}
	
	require plugin_dir_path( __FILE__ ) . 'admin/partials/bold-calculator-admin-display.php';
}

function calculatorPublic() {
    require plugin_dir_path( __FILE__ ) . 'public/partials/bold-calculator-public-display.php';

    return $view;
}

add_shortcode('bold-calculator', 'calculatorPublic');
