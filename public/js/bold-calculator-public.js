const init = () => {
    const submitBtn = document.querySelector('.bcalculator__form-btn--submit');
    const infoContainer = document.querySelector('.bcalculator__info');
    const nameFileld = document.getElementById('bcal-name');
    const nameFileldError = document.querySelector('#bcal-name + .bcalculator__form-input--error');
    const amountFileld = document.getElementById('bcal-amount');
    const amountFileldError = document.querySelector('#bcal-amount + .bcalculator__form-input--error');

    submitBtn.disabled = true;

    const checkFields = () => {
        if (nameFileld.value != '' && amountFileld.value != '') {
            submitBtn.disabled = false;
        } else if (nameFileld.value == '' || amountFileld.value == '') {
            submitBtn.disabled = true;
        } else {
            submitBtn.disabled = true;
        }
    };

    nameFileld.addEventListener('blur', (event) => {
        if (event.target.value == '') {
            nameFileldError.innerText = 'Proszę wprowadzić nazwę produktu';
        } else {
            nameFileldError.innerText = '';
            checkFields();
        }
    });

    amountFileld.addEventListener('blur', (event) => {
        if (event.target.value == '') {
            amountFileldError.innerText = 'Proszę wprowadzić kwotę netto';
        } else {
            amountFileldError.innerText = '';
            checkFields();
        }
    });

    submitBtn.addEventListener('click', (e) => {
        e.preventDefault();

        submitBtn.disabled = true;

        const form = document.getElementById('bcalculator-form');
        const formData = new FormData(form);
        const endpoint = `${document.URL}wp-json/bold-calculator/v1/add-calculation`;
        const amountWithTax = calculateWithTax(formData.get('bcal-amount'), formData.get('bcal-tax-percent'));
        const tax = calculateTax(formData.get('bcal-amount'), formData.get('bcal-tax-percent'));

        sendData(endpoint, Object.fromEntries(formData))
        .then(res => {
            if (res.data) {
                infoContainer.innerText = `Cena produktu ${formData.get('bcal-name')} ,
                wynosi: ${amountWithTax} zł brutto, kwota podatku 
                to ${tax} zł.`;

                submitBtn.disabled = true;
                nameFileld.value = '';
                amountFileld.value = '';
            }
        });
    });
};

const sendData = async function (url = '', data = {}) {
    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });

    return response.json();
}

const calculateWithTax = (amount, tax) => ((+amount * +tax) / 100) + +amount;
const calculateTax = (amount, tax) => ((+amount * +tax) / 100);

window.onload = () => {
    init();
};
