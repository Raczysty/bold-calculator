<?php
$view = '
<div class="bcalculator">
    <h2 class="bcalculator__title">Calculator</h2>
    <h3 class="bcalculator__info"></h3>

    <form id="bcalculator-form" class="bcalculator__form">
        <div class="bcalculator__form-group">
            <label class="bcalculator__form-label" for="bcal-name">
                <span class="bcalculator__form-label-text">
                    Nazwa produktu *
                </span>
            </label>
            <input class="bcalculator__form-input" 
            id="bcal-name" type="text" name="bcal-name"
            placeholder="Proszę podać nazwę produktu" required>
            <span class="bcalculator__form-input--error"></span>
        </div>

        <div class="bcalculator__form-group">
            <label class="bcalculator__form-label" for="bcal-amount">
                <span class="bcalculator__form-label-text">
                    Kwota netto *
                </span>
            </label>
            <input class="bcalculator__form-input" 
            id="bcal-amount" type="number" name="bcal-amount" min="0"
            pattern="[0-9]" placeholder="Proszę podać cenę produktu" required>
            <span class="bcalculator__form-input--error"></span>
        </div>

        <div class="bcalculator__form-group">
            <label class="bcalculator__form-label" for="bcal-tax-percent">
                <span class="bcalculator__form-label-text">
                    Stawka VAT *
                </span>
            </label>
            <select class="bcalculator__form-select" 
            name="bcal-tax-percent" id="">
                <option value="23">23%</option>
                <option value="22">22%</option>
                <option value="8">8%</option>
                <option value="7">7%</option>
                <option value="5">5%</option>
                <option value="3">3%</option>
                <option value="0" selected>0%</option>
            </select>
        </div>

        <div class="bcalculator__form-group bcalculator__form-btn">
            <input type="submit" class="bcalculator__form-btn--submit" 
            value="Oblicz">
        </div>
    </form>
</div>';