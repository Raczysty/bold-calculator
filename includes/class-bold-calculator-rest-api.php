<?php

function addCalculation($request) {
    global $wpdb;

    if ( isset( $request['bcal-name'] ) ) {
        $time = date('Y-m-d H:i:s');
        $ipAddress = getIP();
        $tableName = $wpdb->prefix . 'bold_calculator';

        $productName = clearData($request['bcal-name']);
        $productAmount = clearData($request['bcal-amount']);
        $taxPercent = clearData($request['bcal-tax-percent']);

        $wpdb->query(
            $wpdb->prepare(
                "INSERT INTO $tableName 
                (product_name, amount, tax_percent, ip_address, create_date) 
                VALUES (%s, %d, %d, %s, %s)",
                $productName,
                $productAmount,
                $taxPercent,
                $ipAddress,
                $time
            )
        );

        $newPost = array(
            'post_title' => $productName,
            'post_status' => 'publish',
            'post_date' => $time,
            'post_author' => 1,
            'post_type' => 'bold-calculator',
        );

        $post_id = wp_insert_post($newPost);

        add_post_meta( $post_id, '_amount', $productAmount, true );
        add_post_meta( $post_id, '_tax', $taxPercent, true );
        add_post_meta( $post_id, '_ip', $ipAddress, true );

        $response = array(
            'data' => array(
                'status' => 'success',
                'added' => true,
                'message' => $post_id
            )
        );

        return rest_ensure_response($response);
    }
 
    return new WP_Error( 'rest_invalid', esc_html__( 'The data parameter is required.', 'bold-calculator' ), array( 'status' => 400 ) );
}

function testRestAPI() {
    $response = array(
        'status' => 'success',
        'added' => true,
        'message' => 'Test route'
    );

    return wp_send_json_success($response);
}

function calculateRestRoute() {
    if ( ! function_exists( 'register_rest_route' ) ) {
        return false;
    }

    register_rest_route('bold-calculator/v1', '/add-calculation', 
        array(
            'methods' => WP_REST_Server::EDITABLE,
            'callback' => 'addCalculation',
        )
    );
}

add_action( 'rest_api_init', 'calculateRestRoute' );

function getIP() {
    $ip = '';

    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    return $ip;
}

function clearData($field) {
    $field = trim($field);
    $field = stripslashes($field);
    $field = htmlspecialchars($field);

    return $field;
}