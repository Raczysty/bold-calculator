<?php
function cptCalculator() {
    $supports = array(
        'title',
        'author',
        'custom-fields',
        'revisions',
        'editor'
    );

    $labels = array(
        'name' => _x('Bold Calculator', 'bold-calculator'),
        'singular_name' => _x('Bold Calculator', 'bold-calculator'),
        'menu_name' => _x('Bold Calculator', 'bold-calculator'),
        'name_admin_bar' => _x('Bold Calculator', 'bold-calculator'),
        'add_new' => _x('Dodaj nowy', 'bold-calculator'),
        'add_new_item' => __('Dodaj nowy wpis', 'bold-calculator'),
        'new_item' => __('Nowy wpis', 'bold-calculator'),
        'edit_item' => __('Edytuj wpis', 'bold-calculator'),
        'view_item' => __('Zobacz wpis', 'bold-calculator'),
        'all_items' => __('Wszystkie', 'bold-calculator'),
        'search_items' => __('Szukaj wpisu', 'bold-calculator'),
        'not_found' => __('Nie znaleziono...', 'bold-calculator'),
        'not_found_in_trash' => __('Nie znaleziono w koszu', 'bold-calculator'),
    );

    $args = array(
        'label' => __('Bold Calculator', 'bold-calculator'),
        'labels' => $labels,
        'supports' => $supports,
        'query_var' => true,
        'rewrite' => array('slug' => 'bold-calculator'),
        'has_archive' => true,
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-admin-tools',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true
    );

    register_post_type('bold-calculator', $args);
}

add_action('init', 'cptCalculator');

function calculatorDataBase() {
    global $wpdb;

    $tableName = $wpdb->prefix . 'bold_calculator';
    $query = $wpdb->prepare("SHOW TABLES LIKE %s", $wpdb->esc_like($tableName));

    if ($wpdb->get_var($query) === $tableName) {
        return true;
    }

    $createDDL = "CREATE TABLE $tableName (
        ID bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        product_name text NOT NULL,
        amount bigint(20) UNSIGNED NOT NULL DEFAULT '0',
        tax_percent bigint(20) UNSIGNED NOT NULL DEFAULT '0',
        ip_address text NOT NULL,
        create_date datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
        )";

    $wpdb->query($createDDL);

    if ($wpdb->get_var($query) === $tableName) {
        return true;
    }

    return false;
}

calculatorDataBase();
