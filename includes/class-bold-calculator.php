<?php

class Calculator {
	protected $loader;
	protected $plugin_name;
    protected $version;
    
	public function __construct() {
		if ( defined( 'BOLD_CALC_VERSION' ) ) {
			$this->version = BOLD_CALC_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'bold-calculator';

		$this->load_dependencies();
		$this->define_admin_hooks();
		$this->define_public_hooks();
    }
    
	private function load_dependencies() {
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-bold-calculator-loader.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-bold-calculator-rest-api.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-bold-calculator-admin.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-bold-calculator-public.php';

		$this->loader = new CalculatorLoader();
	}
    
	private function define_admin_hooks() {
		$plugin_admin = new CalculatorAdmin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
    }
    
	private function define_public_hooks() {
		$plugin_public = new CalculatorPublic( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
    }
    
	public function run() {
		$this->loader->run();
    }
    
	public function get_plugin_name() {
		return $this->plugin_name;
    }
    
	public function get_loader() {
		return $this->loader;
    }
    
	public function get_version() {
		return $this->version;
	}
}
